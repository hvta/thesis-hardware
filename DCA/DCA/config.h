#ifndef _CONFIG_H
#define _CONFIG_H

#define SITE_ID "1"
#define ROOM_ID "1"
#define UNIT_ID "1"

#define CLIENT_ID "IND-" SITE_ID "-" ROOM_ID "-" UNIT_ID

#define TOPIC_TEMPLATE "site/" SITE_ID "/room/" ROOM_ID "/unit/" UNIT_ID "/"
#define TOPIC_HEARTBEAT TOPIC_TEMPLATE "heartbeat"
#define TOPIC_STANDBY TOPIC_TEMPLATE "standby"
#define TOPIC_CARD_ID TOPIC_TEMPLATE "cardId"
#define TOPIC_PERSONAL TOPIC_TEMPLATE "personalCode"
#define TOPIC_DOOR_RELEASED TOPIC_TEMPLATE "doorReleased"
#define TOPIC_ACCESS_DENIED TOPIC_TEMPLATE "accessDenied"
#define TOPIC_ACCESS_GRANTED TOPIC_TEMPLATE "accessGranted"
#define TOPIC_WAITING_FOR_VERIFICATION TOPIC_TEMPLATE "waitingForVerification"

const byte mac[] = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0x00 };
const IPAddress server(192, 168, 0, 12);

// LCD
#define LCD_RS_PIN 12
#define LCD_ENABLE_PIN 11
#define LCD_D4_PIN 10
#define LCD_D5_PIN 9
#define LCD_D6_PIN 8
#define LCD_D7_PIN 13

// Keypad
const byte rowPins[] = {7, 6, 5, 4};
const byte colPins[] = { 3, 2, A0, A1 };

const char keys[4][4] = {
	{ '1','2','3', 'A' },
	{ '4','5','6', 'B' },
	{ '7','8','9', 'C' },
	{ '#','0','*', 'D' }
};

/*#define KEYPAD_PIN_1 8
#define KEYPAD_PIN_2 9
#define KEYPAD_PIN_3 10
#define KEYPAD_PIN_4 11
#define KEYPAD_PIN_5 12
#define KEYPAD_PIN_6 13
#define KEYPAD_PIN_7 A0
#define KEYPAD_PIN_8 A1*/

// Electromagnet
#define ELECTROMAGNET_PIN A2

typedef enum {
	typeStandby,
	typeCardId,
	typePersonalCode,
	typeDoorReleased,
	typeAccessDenied,
	typeAccessGranted,
	typeWaitingForVerification
} StateType;

typedef struct {
	StateType type;
	bool oldState;
	bool currentState;
} StateContainer;

#endif