#include <Keypad.h>
#include <PN532_I2C.h>
#include <PN532.h>
#include <Wire.h>
#include <LiquidCrystal.h>
#include <Ethernet.h>
#include <PubSubClient.h>
#include "config.h"

EthernetClient ethClient;
PubSubClient client(ethClient);

StateContainer standby = { typeStandby, true, true };
StateContainer cardId = { typeCardId, false, false };
StateContainer personalCode = { typePersonalCode, false, false };
StateContainer doorReleased = { typeDoorReleased, false, false };
StateContainer accessDenied = { typeAccessDenied, false, false };
StateContainer accessGranted = { typeAccessGranted, false, false };
StateContainer waitingForVerification = { typeWaitingForVerification, false, false };

//LiquidCrystal lcd(LCD_RS_PIN, LCD_ENABLE_PIN, LCD_D4_PIN, LCD_D5_PIN, LCD_D6_PIN, LCD_D7_PIN);

PN532_I2C pn532i2c(Wire);
PN532 nfc(pn532i2c);

Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, 4, 4);

bool handleStateChangeEvent(StateContainer* state, bool newValue, void(*callback)(void))
{
	state->currentState = newValue;

	if (state->currentState != state->oldState)
	{
		if (callback != NULL)
		{
			callback();
		}

		state->oldState = state->currentState;

		return true; // there was a change
	}

	return false; // there was no change
}

unsigned long lastHeartbeat = 0;

void sendHeartbeat()
{
	unsigned long currentHeartbeat = millis();

	if (currentHeartbeat - lastHeartbeat > 10000)
	{
		lastHeartbeat = currentHeartbeat;

		client.publish(TOPIC_HEARTBEAT, "");
	}
}

void callback(char* topic, byte* payload, unsigned int length)
{
	Serial.print("New message: ");
	Serial.print(topic);
	Serial.print(" -> ");
	for (int i = 0; i < length; i++) Serial.print((char)payload[i]);
	Serial.println();

	if (strcmp(topic, TOPIC_STANDBY) == 0)
	{
		standby.currentState = (bool)(payload[0] - '0');
	}
}

void reconnect()
{
	while (!client.connected())
	{
		Serial.print("Connecting to MQTT server... ");

		if (client.connect(CLIENT_ID))
		{
			Serial.println("connected");

			client.subscribe(TOPIC_STANDBY);
			client.subscribe(TOPIC_ACCESS_DENIED);
			client.subscribe(TOPIC_ACCESS_GRANTED);
		}
		else
		{
			Serial.print("failed, rc=");
			Serial.print(client.state());
			Serial.println("try again in 5 seconds");

			delay(5000);
		}
	}
}

void pn532Setup()
{
	nfc.begin();

	uint32_t versiondata = nfc.getFirmwareVersion();

	if (!versiondata)
	{
		Serial.print("Did not find PN53x board");
		while (1);
	}

	nfc.SAMConfig();
}

// Built-in timeout in readPassiveTargetID()
bool lastCardReleased = true;

void pn532Loop()
{
	uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };
	uint8_t uidLength;
	bool cardRead = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength, 500);

	if (cardRead)
	{
		if (lastCardReleased)
		{
			if (uidLength == 4)
			{
				uint32_t cardid = uid[0];

				for (int i = 1; i < uidLength; i++)
				{
					cardid <<= 8;
					cardid |= uid[i];
				}

				Serial.println(cardid);
			}

			lastCardReleased = false;
		}
	}
	else
	{
		lastCardReleased = true;
	}
}

void setup()
{
	Serial.begin(9600);

	//lcd.begin(16, 2);

	pinMode(ELECTROMAGNET_PIN, OUTPUT);

	/*client.setServer(server, 1883);
	client.setCallback(callback);

	Ethernet.begin(mac);

	delay(1500);*/
}

void loop()
{
	char key = keypad.getKey();

	if (key != NO_KEY) {
		//lcd.clear();
		//lcd.print(key);
	}

	Serial.println(key);

	delay(100);

	/*if (!client.connected())
	{
		reconnect();
	}

	client.loop();

	sendHeartbeat();

	if (standby.currentState)
	{
		lcd.clear();  lcd.print("Read Card");
		lcd.clear(); lcd.print("Type PIN");
		lcd.clear(); lcd.print("Access Denied");
		lcd.clear(); lcd.print("Access Granted");
		lcd.clear(); lcd.print("Verify on Web");
		// card module read

		// code provided

		if (handleStateChangeEvent(&cardId, digitalRead(PIR_RX_PIN), NULL))
		{
			client.publish(TOPIC_CARD_ID, cardId.currentState ? "1" : "0");
		}
	}

	delay(100);*/
}
