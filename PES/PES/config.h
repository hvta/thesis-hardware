#ifndef _CONFIG_H
#define _CONFIG_H

#define SITE_ID "1"
#define ROOM_ID "1"
#define UNIT_ID "3"

#define CLIENT_ID "IND-" SITE_ID "-" ROOM_ID "-" UNIT_ID

#define TOPIC_TEMPLATE "site/" SITE_ID "/room/" ROOM_ID "/unit/" UNIT_ID "/"
#define TOPIC_HEARTBEAT TOPIC_TEMPLATE "heartbeat"
#define TOPIC_STANDBY TOPIC_TEMPLATE "standby"
#define TOPIC_SMOKE_DETECTED TOPIC_TEMPLATE "smokeDetected"
#define TOPIC_GAS_DETECTED TOPIC_TEMPLATE "gasDetected"

const byte mac[] = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0x02 };
const IPAddress server(192, 168, 0, 12);

#define SMOKE_PIN A0
#define GAS_PIN A1

const int sensorThreshold = 400;

typedef enum {
	typeStandby,
	typeSmokeDetected,
	typeGasDetected
} StateType;

typedef struct {
	StateType type;
	bool oldState;
	bool currentState;
} StateContainer;

#endif