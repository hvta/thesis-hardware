#include <Ethernet.h>
#include <PubSubClient.h>
#include "config.h"

EthernetClient ethClient;
PubSubClient client(ethClient);

StateContainer standby = { typeStandby, true, true };
StateContainer smokeDetected = { typeSmokeDetected, false, false };
StateContainer gasDetected = { typeGasDetected, false, false };

bool handleStateChangeEvent(StateContainer* state, bool newValue, void(*callback)(void))
{
	state->currentState = newValue;

	if (state->currentState != state->oldState)
	{
		if (callback != NULL)
		{
			callback();
		}

		state->oldState = state->currentState;

		return true; // there was a change
	}

	return false; // there was no change
}

unsigned long lastHeartbeat = 0;

void sendHeartbeat()
{
	unsigned long currentHeartbeat = millis();

	if (currentHeartbeat - lastHeartbeat > 10000)
	{
		lastHeartbeat = currentHeartbeat;

		client.publish(TOPIC_HEARTBEAT, "");
	}
}

void callback(char* topic, byte* payload, unsigned int length)
{
	Serial.print("New message: ");
	Serial.print(topic);
	Serial.print(" -> ");
	for (int i = 0; i < length; i++) Serial.print((char)payload[i]);
	Serial.println();

	if (strcmp(topic, TOPIC_STANDBY) == 0)
	{
		standby.currentState = (bool)(payload[0] - '0');
	}
}

void reconnect()
{
	while (!client.connected())
	{
		Serial.print("Connecting to MQTT server... ");

		if (client.connect(CLIENT_ID))
		{
			Serial.println("connected");

			client.subscribe(TOPIC_STANDBY);
		}
		else
		{
			Serial.print("failed, rc=");
			Serial.print(client.state());
			Serial.println("try again in 5 seconds");

			delay(5000);
		}
	}
}

void setup()
{
	Serial.begin(9600);

	pinMode(SMOKE_PIN, OUTPUT);

	pinMode(GAS_PIN, OUTPUT);

	client.setServer(server, 1883);
	client.setCallback(callback);

	Ethernet.begin(mac);

	delay(1500);
}

void loop()
{
	if (!client.connected())
	{
		reconnect();
	}

	client.loop();

	sendHeartbeat();

	if (standby.currentState)
	{
		int sensorVal = analogRead(SMOKE_PIN);
		bool aboveThreshold = sensorVal > sensorThreshold;

		if (handleStateChangeEvent(&smokeDetected, aboveThreshold, NULL))
		{
			client.publish(TOPIC_SMOKE_DETECTED, smokeDetected.currentState ? "1" : "0");
		}

		sensorVal = analogRead(GAS_PIN);
		aboveThreshold = sensorVal > sensorThreshold;

		if (handleStateChangeEvent(&gasDetected, aboveThreshold, NULL))
		{
			client.publish(TOPIC_GAS_DETECTED, gasDetected.currentState ? "1" : "0");
		}
	}

	delay(100);
}
