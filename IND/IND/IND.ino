#include <Ethernet.h>
#include <PubSubClient.h>
#include "config.h"

EthernetClient ethClient;
PubSubClient client(ethClient);

StateContainer standby = {typeStandby, true, true};
StateContainer irDetected = {typeIrDetected, false, false};
StateContainer pirDetected = {typePirDetected, false, false};

bool handleStateChangeEvent(StateContainer* state, bool newValue, void (*callback)(void))
{
	state->currentState = newValue;

	if (state->currentState != state->oldState)
	{
		if (callback != NULL)
		{
			callback();
		}
		
		state->oldState = state->currentState;

		return true; // there was a change
	}

	return false; // there was no change
}

unsigned long lastHeartbeat = 0;

void sendHeartbeat()
{
	unsigned long currentHeartbeat = millis();

	if (currentHeartbeat - lastHeartbeat > 10000)
	{
		lastHeartbeat = currentHeartbeat;

		client.publish(TOPIC_HEARTBEAT, "");
	}	
}

void callback(char* topic, byte* payload, unsigned int length)
{
	Serial.print("New message: ");
	Serial.print(topic);
	Serial.print(" -> ");
	for (int i = 0; i < length; i++) Serial.print((char)payload[i]);
	Serial.println();

	if (strcmp(topic, TOPIC_STANDBY) == 0)
	{
		standby.currentState = (bool)(payload[0] - '0');
	}
}

void reconnect()
{
	while (!client.connected())
	{
		Serial.print("Connecting to MQTT server... ");

		if (client.connect(CLIENT_ID))
		{
			Serial.println("connected");
			
			client.subscribe(TOPIC_STANDBY);
		}
		else
		{
			Serial.print("failed, rc=");
			Serial.print(client.state());
			Serial.println("try again in 5 seconds");
			
			delay(5000);
		}
	}
}

void setup()
{
	Serial.begin(9600);

	pinMode(PIR_RX_PIN, INPUT);

	pinMode(IR_TX_PIN, OUTPUT);
	pinMode(IR_RX_PIN, INPUT);

	client.setServer(server, 1883);
	client.setCallback(callback);

	Ethernet.begin(mac);

	delay(1500);
}

void loop()
{
	if (!client.connected())
	{
		reconnect();
	}

	client.loop();

	sendHeartbeat();

	if (standby.currentState)
	{
		/*if (handleStateChangeEvent(&irDetected, digitalRead(IR_RX_PIN), NULL))
		{
			client.publish(TOPIC_IR_DETECTED, irDetected.currentState ? "1" : "0");
		}*/

		if (handleStateChangeEvent(&pirDetected, digitalRead(PIR_RX_PIN), NULL))
		{
			client.publish(TOPIC_PIR_DETECTED, pirDetected.currentState ? "1" : "0");
		}
	}

	delay(100);
}
