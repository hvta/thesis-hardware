#ifndef _CONFIG_H
#define _CONFIG_H

#define SITE_ID "1"
#define ROOM_ID "1"
#define UNIT_ID "2"

#define CLIENT_ID "IND-" SITE_ID "-" ROOM_ID "-" UNIT_ID

#define TOPIC_TEMPLATE "site/" SITE_ID "/room/" ROOM_ID "/unit/" UNIT_ID "/"
#define TOPIC_HEARTBEAT TOPIC_TEMPLATE "heartbeat"
#define TOPIC_STANDBY TOPIC_TEMPLATE "standby"
#define TOPIC_IR_DETECTED TOPIC_TEMPLATE "irDetected"
#define TOPIC_PIR_DETECTED TOPIC_TEMPLATE "pirDetected"

const byte mac[] = {0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0x01};
const IPAddress server(192, 168, 0, 12);

#define PIR_RX_PIN 8

#define IR_TX_PIN 6
#define IR_RX_PIN 7

typedef enum {
	typeStandby,
	typeIrDetected,
	typePirDetected
} StateType;

typedef struct {
	StateType type;
	bool oldState;
	bool currentState;
} StateContainer;

#endif