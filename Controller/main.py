import init
import mqtt
import websocket
import _thread


def host_mqtt():
    m2m = mqtt.Mqtt()
    m2m.start()


def host_websocket():
    websocket.tornado.options.parse_command_line()
    ws = websocket.Application()
    ws.listen(websocket.options.port)
    websocket.tornado.ioloop.IOLoop.current().start()


def main():
    init.construct_units()

    try:
        _thread.start_new_thread(host_mqtt, ())
    except Exception as e:
        print(e)
        exit(-1)

    host_websocket()


if __name__ == "__main__":
    main()
