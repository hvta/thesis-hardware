import mysql.connector
import config


class Database:
    def __init__(self):
        self.server = config.get_db('SERVER')
        self.username = config.get_db('USERNAME')
        self.password = config.get_db('PASSWORD')
        self.database = config.get_db('DATABASE')

        self.conn = mysql.connector.connect(
            host=self.server,
            user=self.username,
            passwd=self.password,
            database=self.database
        )

        self.cursor = self.conn.cursor()

    def close_conn(self):
        self.conn.close()

    def execute(self, cmd):
        self.cursor.execute(cmd)

    def commit(self):
        self.conn.commit()

    def fetch_result(self):
        return self.cursor.fetchall()

    def last_row_id(self):
        return self.cursor.lastrowid

