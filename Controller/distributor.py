message_queue = []


def add_to_message_queue(data):
    global message_queue

    message_queue.append(data)


def check_message():
    global message_queue

    if not message_queue:
        return None

    return message_queue.pop(0)
