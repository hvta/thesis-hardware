import tornado.ioloop
import tornado.options
import tornado.websocket
import tornado.web
import tornado.escape
from tornado.options import define, options
import logging
import uuid
import config
import distributor

define("port", default=config.get_websocket('PORT'), help="run on the given port", type=int)


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [(r"/hossocket", HosSocketHandler)]
        settings = dict(
            xsrf_cookies=False,
        )
        super(Application, self).__init__(handlers, **settings)

        callback = tornado.ioloop.PeriodicCallback(HosSocketHandler.on_global_check, 100.0)
        callback.start()


class HosSocketHandler(tornado.websocket.WebSocketHandler):
    clients = set()

    def check_origin(self, origin):
        return True

    def open(self):
        HosSocketHandler.clients.add(self)

    def on_close(self):
        HosSocketHandler.clients.remove(self)

    def on_message(self, message):
        logging.info("got message %r", message)
        try:
            parsed = tornado.escape.json_decode(message)
            data = parsed["data"]
            # HosSocketHandler.send_updates(data)
        except:
            logging.error("Error parsing message", exc_info=True)

    @classmethod
    def send_message(cls, data):
        logging.info("sending message to %d clients", len(cls.clients))
        for client in cls.clients:
            try:
                client.write_message(data)
            except:
                logging.error("Error sending message", exc_info=True)

    @classmethod
    def on_global_check(cls):
        data = distributor.check_message()

        if data is not None:
            cls.send_message(data)
