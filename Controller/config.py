import configparser


config = configparser.ConfigParser()
config.read('config.ini')


def get_db(var):
    return config['DB'][var]


def get_mqtt(var):
    return config['MQTT'][var]


def get_websocket(var):
    return config['WEBSOCKET'][var]
