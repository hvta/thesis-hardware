import database


# read user is active
# read user has permission to room
# write system event log
# read user card
# read personal code
# write verifications
# read verifications


def get_system_units():
    db = database.Database()
    sql = """SELECT rooms.site_id AS site_id, rooms.id AS room_id, system_units.id AS unit_id,
            system_units.unit_type, system_units.is_active
            FROM system_units
            JOIN rooms ON system_units.room_id = rooms.id;"""
    db.execute(sql)
    res = db.fetch_result()
    db.close_conn()

    return res


def get_user_by_id(user_id):
    db = database.Database()
    sql = """SELECT id, card_id, personal_code, is_active, firstname
            FROM users
            WHERE id = %s;"""
    db.execute(sql % user_id)
    res = db.fetch_result()
    db.close_conn()

    return res


def get_user_by_card(card_id):
    db = database.Database()
    sql = """SELECT id, card_id, personal_code, is_active, firstname 
            FROM users
            WHERE card_id = '%s';"""
    db.execute(sql % card_id)
    res = db.fetch_result()
    db.close_conn()

    return res


def user_has_permission_to_room(user_id, room_id):
    db = database.Database()
    sql = """SELECT user_id, room_id
            FROM room_access_permissions
            WHERE user_id = %s AND room_id = %s;"""
    db.execute(sql % (user_id, room_id))
    res = db.fetch_result()
    db.close_conn()

    return res


def user_verified_access(user_id, system_unit_id, token):
    db = database.Database()
    sql = """SELECT system_unit_id, user_id, expires_at
            FROM verifications
            WHERE user_id = %s AND system_unit_id = %s AND token = '%s';"""
    db.execute(sql % (user_id, system_unit_id, token))
    res = db.fetch_result()
    db.close_conn()

    return res


def write_verification(user_id, system_unit_id, token, expires_at):
    db = database.Database()
    sql = """INSERT INTO verifications (system_unit_id, user_id, token, expires_at)
            VALUES (%s, %s, '%s', '%s');"""
    db.execute(sql % (user_id, system_unit_id, token, expires_at))
    db.commit()
    res = db.last_row_id()
    db.close_conn()

    return res


def write_system_event_log(system_unit_id, event_type, description, user_id=None):
    db = database.Database()
    sql = """INSERT INTO system_event_logs (system_unit_id, event_type, description, user_id)
                VALUES (%s, '%s', '%s', %s);"""
    db.execute(sql % (system_unit_id, event_type, description, user_id if user_id is not None else 'NULL'))
    db.commit()
    res = db.last_row_id()
    db.close_conn()

    return res
