import json
import random
from systemunit.systemunit import SystemUnit


class Dca(SystemUnit):
    def __init__(self, site_id, room_id, unit_id, unit_type):
        SystemUnit.__init__(self, site_id, room_id, unit_id, unit_type)

        self.token = None
        self.card_id = None

    def entry_route(self, resource, payload):
        super().entry_route(resource, payload)

    def generate_verification_token(self):
        self.token = random.getrandbits(128)

    def get_verification_token(self):
        return self.token

    def set_card_id(self, card_id):
        self.card_id = card_id

    def get_card_id(self,):
        return self.card_id

    def set_personal_code(self, personal_code):
        self.personal_code = personal_code

    def get_personal_code(self):
        return self.personal_code