import json
from systemunit.systemunit import SystemUnit


class Pes(SystemUnit):
    def __init__(self, site_id, room_id, unit_id, unit_type):
        SystemUnit.__init__(self, site_id, room_id, unit_id, unit_type)

        self.smoke_detected = False
        self.gas_detected = False

        # last detection system_event_log IDs
        self.smoke_detection_log_id = 0
        self.gas_detection_log_id = 0

    def entry_route(self, resource, payload):
        super().entry_route(resource, payload)

        if resource == "smokeDetected" or resource == "gasDetected":
            if int(payload) == 1:
                if resource == "smokeDetected":
                    self.smoke_detected = True
                elif resource == "gasDetected":
                    self.gas_detected = True
                self.process_detection(resource)
            elif int(payload) == 0:
                if resource == "smokeDetected":
                    self.smoke_detected = False
                elif resource == "gasDetected":
                    self.gas_detected = False
                self.process_end_detection(resource)

    def process_detection(self, resource):
        json_data = self.get_json_template()

        rowid = self.write_log_to_db(self.unit_id, "alert", "alert, unit ID:{}".format(self.unit_id))

        if resource == "smokeDetected":
            self.smoke_detection_log_id = rowid
        elif resource == "gasDetected":
            self.gas_detection_log_id = rowid

        json_data["type"] = "alert"
        json_data["data"] = {
            "system_event_log_id": rowid,
            "site_id": self.site_id,
            "room_id": self.room_id,
            "unit": {
                "id": self.unit_id,
                "type": self.unit_type,
                "alert_triggered_by": resource
            }
        }

        self.add_to_message_queue(json.dumps(json_data))

    def process_end_detection(self, resource):
        json_data = self.get_json_template()

        json_data["type"] = "alert_end"
        json_data["data"] = {
            "system_event_log_id":
                self.smoke_detection_log_id
                if resource == "smokeDetected" else self.gas_detection_log_id,
        }

        self.add_to_message_queue(json.dumps(json_data))

        if resource == "smokeDetected":
            self.smoke_detection_log_id = 0
        elif resource == "gasDetected":
            self.gas_detection_log_id = 0