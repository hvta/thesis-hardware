import mqtt
import dbcommandlib
import time
import json
import distributor

topic_template = """site/{}/room/{}/unit/{}/{}"""

topic_resources = {
    'DCA': [
        'standby',
        'cardId',
        'personalCode',
        'doorReleased',
        'accessDenied',
        'accessGranted',
        'waitingForVerification',
        'heartbeat'
    ],
    'IND': [
        'standby',
        'irDetected',
        'pirDetected',
        'heartbeat'
    ],
    'PES': [
        'standby',
        'smokeDetected',
        'gasDetected',
        'heartbeat'
    ],
    'CCS': []
}

json_template = {
    "type": "",
    "data": {}
}

system_units = []


class SystemUnit:
    def __init__(self, site_id, room_id, unit_id, unit_type):
        self.site_id = site_id
        self.room_id = room_id
        self.unit_id = unit_id
        self.unit_type = unit_type
        self.topics = []
        self.last_heartbeat_ts = 0

        self.generate_topics(unit_type)

    def entry_route(self, resource, payload):
        if resource == "heartbeat":
            self.process_heartbeat()
            return

    def get_id(self):
        return self.unit_id

    def process_heartbeat(self):
        global json_template

        self.last_heartbeat_ts = time.time()

        rowid = self.write_log_to_db(self.unit_id, "info", "heartbeat, unit ID:{}".format(self.unit_id))

        json_data = json_template

        json_data["type"] = "heartbeat"
        json_data["data"] = {
            "system_event_log_id": rowid,
            "site_id": self.site_id,
            "room_id": self.room_id,
            "unit_id": self.unit_id,
            "unit_type": self.unit_type,
            "heartbeat_ts": self.last_heartbeat_ts
        }

        self.add_to_message_queue(json.dumps(json_data))

    def generate_topics(self, unit_type):
        global topic_resources

        for resource_name in topic_resources[unit_type]:
            self.topics.append(topic_template.format(self.site_id, self.room_id, self.unit_id, resource_name))

    def mqtt_publish(self, client, topic, payload, qos=0, retain=False):
        client.publish(topic, payload, qos, retain)

    def write_log_to_db(self, system_unit_id, event_type, description, user_id=None):
        return dbcommandlib.write_system_event_log(system_unit_id, event_type, description, user_id)

    def add_to_message_queue(self, data):
        distributor.add_to_message_queue(data)

    def get_json_template(self):
        global json_template

        return json_template
