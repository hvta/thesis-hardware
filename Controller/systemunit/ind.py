import json
from systemunit.systemunit import SystemUnit


class Ind(SystemUnit):
    def __init__(self, site_id, room_id, unit_id, unit_type):
        SystemUnit.__init__(self, site_id, room_id, unit_id, unit_type)

        self.ir_detected = False
        self.pir_detected = False

        # last detection system_event_log IDs
        self.ir_detection_log_id = 0
        self.pir_detection_log_id = 0

    def entry_route(self, resource, payload):
        super().entry_route(resource, payload)

        if resource == "irDetected" or resource == "pirDetected":
            if int(payload) == 1:
                if resource == "irDetected":
                    self.ir_detected = True
                elif resource == "pirDetected":
                    self.pir_detected = True
                self.process_detection(resource)
            elif int(payload) == 0:
                if resource == "irDetected":
                    self.ir_detected = False
                elif resource == "pirDetected":
                    self.pir_detected = False
                self.process_end_detection(resource)

    def process_detection(self, resource):
        json_data = self.get_json_template()

        rowid = self.write_log_to_db(self.unit_id, "alert", "alert, unit ID:{}".format(self.unit_id))

        if resource == "irDetected":
            self.ir_detection_log_id = rowid
        elif resource == "pirDetected":
            self.pir_detection_log_id = rowid

        json_data["type"] = "alert"
        json_data["data"] = {
            "system_event_log_id": rowid,
            "site_id": self.site_id,
            "room_id": self.room_id,
            "unit": {
                "id": self.unit_id,
                "type": self.unit_type,
                "alert_triggered_by": resource
            }
        }

        self.add_to_message_queue(json.dumps(json_data))

    def process_end_detection(self, resource):
        json_data = self.get_json_template()

        json_data["type"] = "alert_end"
        json_data["data"] = {
            "system_event_log_id":
                self.ir_detection_log_id
                if resource == "irDetected" else self.pir_detection_log_id,
        }

        self.add_to_message_queue(json.dumps(json_data))

        if resource == "irDetected":
            self.ir_detection_log_id = 0
        elif resource == "pirDetected":
            self.pir_detection_log_id = 0
