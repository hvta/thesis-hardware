import paho.mqtt.client as mqtt
import config
import distributor


def get_unit_by_topic(topic):
        pieces = topic.split('/')
        unit_id = int(pieces[5])

        from systemunit.systemunit import system_units
        for unit in system_units:
            if unit.get_id() == unit_id:
                return unit

        return None


def get_resource_by_topic(topic):
    return topic.split('/')[-1]


class Mqtt:
    def __init__(self):
        self.client = mqtt.Client()
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message

        self.client.connect(config.get_mqtt('BROKER_ADDRESS'),
                            int(config.get_mqtt('PORT')),
                            int(config.get_mqtt('KEEP_ALIVE')))

    def on_connect(self, client, userdata, flags, rc):
        print("Connected with result code " + str(rc))

        self.client.subscribe("#")

    def on_message(self, client, userdata, msg):
        print(msg.topic + " " + str(msg.payload))

        unit = get_unit_by_topic(msg.topic)

        if unit is not None:
            unit.entry_route(get_resource_by_topic(msg.topic), msg.payload.decode("utf-8"))

        # distributor.add_to_message_queue(msg.payload)

    def start(self):
        # Blocking call that processes network traffic, dispatches callbacks and
        # handles reconnecting.
        # Other loop*() functions are available that give a threaded interface and a
        # manual interface.

        self.client.loop_forever()
