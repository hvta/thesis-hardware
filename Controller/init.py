import dbcommandlib
from systemunit.dca import Dca
from systemunit.ind import Ind
from systemunit.pes import Pes
from systemunit.systemunit import system_units


def construct_units():
    res = dbcommandlib.get_system_units()

    for (site_id, room_id, unit_id, unit_type, is_active) in res:
        if is_active == 0:
            continue
        if unit_type == 'DCA':
            system_units.append(Dca(site_id, room_id, unit_id, unit_type))
        elif unit_type == 'IND':
            system_units.append(Ind(site_id, room_id, unit_id, unit_type))
        elif unit_type == 'PES':
            system_units.append(Pes(site_id, room_id, unit_id, unit_type))
